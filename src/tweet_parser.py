from classes.PlayerList import PlayerList
from configuration import twitter, api
from classes.notify import Notify
from models.Tweet import Tweet
import dataset
import time
import string
from db_connect import db

__DEBUG__ = False

tweets = db['tweets']
players = db['players']
users = db['users']

players.delete()
player_list = PlayerList(db)
player_list.refresh()

notify=Notify(api.twitter_api_key, api.twitter_secret_key)

def parse_tweets():
	tweet_list = Tweet(_engine=db)

	while True:
		if __DEBUG__:
			print "Checking for new tweets..."
		try:
			new_tweets = tweet_list.find(status='0')
		except Exception as e:
			new_tweets = []

		for tweet in new_tweets:
			save = False

			#remove punctuation
			text = "".join(l for l in tweet.text if l not in string.punctuation).lower()

			#check for our filter keywords in the tweet
			if any(word in text for word in twitter.filters):
				for player in player_list.players:

					name = player.name.lower().split()

					#check for player names in tweet
					if all(word in text for word in name):
						if __DEBUG__:
							print 'Tweet injury detected - [' + tweet.text + ']'
						else: 
							save = True #save the tweet
							#notify the player every time, for debug
							notify.notify(tweet.text, '+12085059660', '+12089916400') 

							#check whether we should notify the  player
							if player.notify_increment >= 3:
								player.notify_increment = 0
								player.save()
							else:
								player.notify_increment += 1
								player.save()

			#check whether we should keep the tweet or not
			if save:
				tweet.status = 1
				tweet.save()
				print tweet.text
			else:
				tweet.delete()

		time.sleep(15)

parse_tweets()
