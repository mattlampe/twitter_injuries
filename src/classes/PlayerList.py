__author__ = 'Daniel'
import requests
from models.Player import Player
from xml.etree import ElementTree


class PlayerList():
	players = []
	url = "http://api.cbssports.com/fantasy/players/list?version=3.0&SPORT=football"
	db = 0

	def __init__(self, db):
		self.db = db
		pass

	def refresh(self):
		response = requests.get(self.url)
		body = ElementTree.fromstring(response.content)
		for child in body[0][0]:
			player = Player(self.db)
			player.name = str(child.find('firstname').text) + " " + str(child.find('lastname').text)
			player.position = str(child.find('position').text)
			player.save()
			self.players.append(player)
	
	def print_list(self):
		print self.players
