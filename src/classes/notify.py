__author__ = 'Daniel'
from twilio.rest import TwilioRestClient


#This is a basic listener that just prints received tweets to stdout.
class Notify():
    client = 0

    def __init__(self, account, token):
        self.client = TwilioRestClient(account, token)
        pass

    def notify(self, text, from_, to_):
		try: 
			self.client.messages.create(
				to_=to_,
				from_=from_,
				body_=text
			)
		except twilio.TwilioRestException as e:
			print e

