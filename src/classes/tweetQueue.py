__author__ = 'Daniel'
from collections import deque


#This is a basic listener that just prints received tweets to stdout.
class TweetQueue():
    tweetQueue = deque()

    def __init__(self):
        self.tweetQueue = deque()
        pass

    def stash(self, player):
        for stashedPlayer in self.tweetQueue:
            if player == stashedPlayer:
                stashedPlayer+=1
                return True

        self.tweetQueue.append(player)
        return True
