filters = [
    'injury',
    'injured',
    'hurt',
    'sprain',
    'ankle',
    'back',
    'leg',
    'foot',
    'knee',
    'arm',
    'groin',
    'wrist',
    'arm',
    'back',
    'return',
    'returns',
    'expected',
    'better',
    'rejoin',
    'rejoins',
    'recover',
    'recovers',
    'recovered',
    'returns',
    'returns',
    'practice'
]

follow = [
    '1649487446',
    '24277551',
    '16672159',
    '645043',
    '1861287751',
    '395509552',
    '901837542',
    '1939860914',
    '863420437',
    '51263592',
    '810094262',
    '16403943',
    '581971753',
    '40884433',  #@david_c_steele
    '2982393346',  #@fantasysourcefb
    '1148215939',  #@jason_otc
    '39129998',  #@optimumscouting
    '3910424235',  #@adbrandt
    '21330413',  #@andrew_garda
    '42670679',  #@ashleyfoxespn
    '21897334',  #@billbarnwell
    '39977870',  #@bobglauber
    '15729027',  #@caplannfl
    '36362259',  #claytonespn
    '41859939', #dameshek
    '85654185', #danpompei
    '19728661', #daringantt
    '34722669', #donbanks
    '16348640', #espngraziano
    '58919137', #fieldyates
    '83888782', #fo_aschatz
    '17172869', #gregabedard
    '24903274', #greggrosenthal
    '15398442', #izzygould
    '14892214', #janesports
    '48492831', #jasonlacanfora
    '4023674493', #jasonphilcole
    '55307193', #jayglazer
    '26144265', #jeffdarlington#
    '31941143', #jennyvrentas#
    '32134081', #jimtrotter_nfl#
    '16402282', #joefortenbaugh#
    '53165610', #joshalper
    '31150483', #joshkatzowitz#
    '71619021', #judybattista#
    '838333813'
]
