import dataset 
import os

db_address = os.environ['POSTGRES_PORT_5432_TCP_ADDR']
db_port = os.environ['POSTGRES_PORT_5432_TCP_PORT']

db = dataset.connect('postgresql://{}:{}/main'.format(db_address, db_port))

