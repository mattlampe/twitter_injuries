#imports
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import configuration.api
import configuration.twitter
from db_connect import db
import json
from models.Tweet import Tweet

class StdOutListener(StreamListener):
    def __init__(self):
        super(StdOutListener, self).__init__()

    def on_data(self, data):
        try:
            tweet_data = json.loads(data)
            if isinstance(tweet_data, dict):
                if not tweet_data['retweeted'] and 'RT @' not in tweet_data['text']:
                    tweet_model = Tweet(db)
                    tweet_model.text = tweet_data['text']
                    tweet_model.status = 0
                    tweet_model.save()
                    print tweet_model.text
        except ValueError, e:
            pass

    def on_error(self, status):
        print status

if __name__ == '__main__':
    l = StdOutListener()
    auth = OAuthHandler(configuration.api.consumer_key, configuration.api.consumer_secret)
    auth.set_access_token(configuration.api.access_token, configuration.api.access_token_secret)
    stream = Stream(auth, l)

    stream.filter(follow=configuration.twitter.follow)
