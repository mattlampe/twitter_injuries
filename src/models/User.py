from models import Model


class User(Model):
	_table = 'users'
	_fields = [
		('username', ""),
		('phoneNumber', 0),
		('email', 0),
		('name', 0)
	]
	_indexes = ['id']
