from models import Model


class Tweet(Model):
	_table = 'tweets'
	_fields = [
		('account', 0),
		('status', 0),
		('text', '')
	]
	_indexes = ['id']
