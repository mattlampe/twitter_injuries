from models import Model

class Player(Model):
	_table = 'players'
	_fields = [
		('name', ''),
		('notify_increment', 0),
		('position', '')
	]
	_indexes = ['id']

