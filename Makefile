ROOT=$(shell pwd)
SOURCE_DIR=$(ROOT)/src
DATABASE_DIR=$(ROOT)/database
COMPOSE_DIR=$(ROOT)/build
IMAGES_DIR=$(COMPOSE_DIR)/images

docker-start: 
	mkdir -p $(IMAGES_DIR)/tweet_parser/src
	cp -r $(SOURCE_DIR)/* $(IMAGES_DIR)/tweet_parser/src
	cp $(DATABASE_DIR)/migrate.sql $(IMAGES_DIR)/postgres/migrate.sql
	docker-compose -f $(COMPOSE_DIR)/tweet-parser.yml up -d

docker-build:
	docker-compose -f $(COMPOSE_DIR)/tweet-parser.yml build tweetparser

docker-start-postgres:
	docker run -d postgres:9.4

docker-stop:
	docker-compose -f $(COMPOSE_DIR)/tweet-parser.yml stop

docker-clean: 
	docker-compose -f $(COMPOSE_DIR)/tweet-parser.yml rm -v --force
	docker rmi build_tweetparser
	docker rmi build_postgres

# remove any untagged images
docker-prune:
	docker rmi $(docker images | grep "^<none>" | awk "{print $3}") 

clean:
	rm -rf $(IMAGES_DIR)/tweet_parser/src
