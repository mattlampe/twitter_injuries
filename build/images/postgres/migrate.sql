CREATE DATABASE main;
\c main;

CREATE TABLE IF NOT EXISTS players (
	id					SERIAL PRIMARY KEY,
	name				TEXT,
	notify_increment	INT NOT NULL DEFAULT 0,
	position			TEXT);

CREATE TABLE IF NOT EXISTS tweets (
	id					SERIAL PRIMARY KEY,
	account				INT NOT NULL DEFAULT 0,
	status				INT NOT NULL DEFAULT 0,
	text				CHAR(140));

CREATE TABLE IF NOT EXISTS users (
	id					SERIAL PRIMARY KEY,
	username			TEXT,
	phone_number		INT,
	email				TEXT,
	name				TEXT);
